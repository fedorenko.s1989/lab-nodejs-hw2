const express = require('express');
const path = require('path');
const morgan = require('morgan');
const mongoose = require('mongoose');
const HOST = require('dotenv').config();
const app = express();

const { notesRouter } = require('./controllers/notesController');
const { authRouter } = require('./controllers/authController');
const { usersRouter } = require('./controllers/usersController');
const {authMiddleware} = require('./middlewares/authMiddleware');
const {Hw2Error} = require('./utils/errors');

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/auth', authRouter);

app.use('/api/notes', [authMiddleware], notesRouter);
app.use('/api/users', [authMiddleware], usersRouter);

app.use((req, res, next) => {
    res.status(404).json({message: err.message});
});

app.use((err, req, res, next) => {
    if (err instanceof Hw2Error) {
        return res.status(err.status).json({message: err.message});
    }
    res.status(500).json({message: err.message});
});

const start = async () => {
    try {
        await mongoose.connect('mongodb+srv://SashaF:SFtest@cluster0.cocva.mongodb.net/hw2?retryWrites=true&w=majority', 
        {useNewUrlParser: true, useUnifiedTopology: true});

        app.listen(HOST.parsed.PORT);
    } catch (err) {
        console.error(`Server error: ${err.message}`);
    }
    
}

start();