class Hw2Error extends Error {
    constructor (message) {
        super(message);
        this.status = 500;
    }
}

class InvalidRequestError extends Hw2Error {
    constructor (message = 'Invalid request') {
        super(message);
        this.status = 400;
    }
}

class CredentialsError extends Hw2Error {
    constructor (message = 'Invalid credentials') {
        super(message);
        this.status = 400;
    }
}

class ValidationError extends Hw2Error {
    constructor (message = 'Invalid request registarion') {
        super(message);
        this.status = 400;
    }
}

module.exports = {
    Hw2Error,
    InvalidRequestError,
    CredentialsError,
    ValidationError
}