const { Note } = require('../models/noteModel');

const getNotesByUserId = async (userId, limit=0, offset=0) => {
    const notes = await Note.find({userId}, '-__v')
        .skip(offset)
        .limit(limit);   
    return notes; 
}

const addNotes = async (userId, notePayload) => {
    const note = new Note({...notePayload, userId});
    await note.save();
}

const getNoteByIdForUser = async (noteId, userId) => {
    const note = await Note.findByIdAndUpdate({_id: noteId, userId}, {$set: {completed: true}});
    return await Note.findOne({_id: noteId, userId}, '-__v');;
}

const updateNoteByIdForUser = async (noteId, userId, text) => {
    await Note.findByIdAndUpdate({_id: noteId, userId}, { $set: {text: text}});
}

const toggleCompletedForUserNoteById = async (noteId, userId) => {
    const note = await Note.findOne({_id: noteId, userId});

    if (note.completed !== true) {
        await note.update({$set: {completed: true}});
    }
    await note.update({$set: {completed: false}});
}

const deleteNoteByIdForUser = async (noteId, userId) => {
    await Note.findOneAndRemove({_id: noteId, userId});
}

module.exports = {
    getNotesByUserId,
    addNotes,
    getNoteByIdForUser,
    updateNoteByIdForUser,
    toggleCompletedForUserNoteById,
    deleteNoteByIdForUser
}