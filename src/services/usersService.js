const bcrypt = require('bcrypt');
const { User } = require('../models/userModel');

const getUserByUserId = async (userId) => {
    const user = await User.findOne({_id: userId}, '-__v -password');   
    return user; 
}

const deleteUserById = async (userId) => {
    await User.findOneAndRemove({_id: userId});
}

const changeUserPasswordById = async (userId, oldPassword, newPassword) => {
    const user = await User.findOne({_id: userId});
    
    if (!(await bcrypt.compare(oldPassword, user.password))) {
        throw new Error('Invalid oldpassword');
    }
    
    await User.findByIdAndUpdate({_id: userId}, {$set: {password: await bcrypt.hash(newPassword, 10)}});
}

module.exports = {
    getUserByUserId,
    deleteUserById,
    changeUserPasswordById
}