const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const { User } = require('../models/userModel');

const { CredentialsError } = require('../utils/errors');

const registration = async ({username, password}) => {
    const user = new User({
        email: `${username}@email.com`, 
        username,
        password: await bcrypt.hash(password, 10)
    });
    
    await user.save();
}

const logIn = async ({username, password}) => {
    const user = await User.findOne({username});

    if (!user) {
        throw new CredentialsError('Invalid password or username');
    }

    if (!(await bcrypt.compare(password, user.password))) {
        throw new CredentialsError('Invalid password or username');
    }

    const token = jwt.sign({
        _id: user._id,
        username: user.username
    }, 'secret');
    return token;
}

module.exports = {
    registration,
    logIn
}