const jwt = require('jsonwebtoken');

const { CredentialsError } = require('../utils/errors');

const authMiddleware = (req, res, next) => {
    const  {
        authorization
    } = req.headers;

    if (!authorization) {
        throw new CredentialsError('Plese, provide "authorization" header');
    }

    const [, token] = authorization.split(' ');

    if (!token) {
        throw new CredentialsError('Plese, include token to request');
    }

    try {
        const tokenPayload = jwt.verify(token, 'secret');
        req.user = {
            userId: tokenPayload._id,
            username: tokenPayload.username  
        }
        next();
    } catch (err) {
        return res.status(401).json({message: err.message});
    }

}

module.exports = {
    authMiddleware
}