const express = require('express');
const router = express.Router();

const { 
    getUserByUserId,
    deleteUserById,
    changeUserPasswordById
 } = require('../services/usersService');

const { asyncWrapper } = require('../utils/apiUtils');

const { InvalidRequestError } = require('../utils/errors');

router.get('/me', asyncWrapper(async (req, res) => {
    const { userId } = req.user;

    const user = await getUserByUserId(userId);
    if(!user) {
        throw new InvalidRequestError('No users found');
    }
    res.json({user});
}));

router.delete('/me', asyncWrapper(async (req, res) => {
    const { userId } = req.user;
    
    await deleteUserById(userId);
    
    res.json({message: 'Success'});
}));

router.patch('/me', asyncWrapper(async (req, res) => {
    const { userId } = req.user;

    const { oldPassword, newPassword } = req.body;

    await changeUserPasswordById(userId, oldPassword, newPassword);
    res.json({message: 'Success'});
}));


module.exports = {
    usersRouter: router
}

