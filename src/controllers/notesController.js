const express = require('express');
const { Note } = require('../models/noteModel');
const router = express.Router();

const { 
    getNotesByUserId,
    addNotes,
    getNoteByIdForUser,
    updateNoteByIdForUser,
    toggleCompletedForUserNoteById,
    deleteNoteByIdForUser
 } = require('../services/notesService');

const { asyncWrapper } = require('../utils/apiUtils');

const { InvalidRequestError } = require('../utils/errors');

router.get('/', asyncWrapper(async (req, res) => {
    const { userId } = req.user;
    const limit = 0;
    const offset = 0;
    
    if (req.params.offset) {
        offset = req.params.offset;
    }
    if (req.params.limit) {
        limit = req.params.limit;
    }

    const notes = await getNotesByUserId(userId, limit, offset);
    
    res.json({
        offset: offset, 
        limit: limit,
        count: await Note.find().estimatedDocumentCount(),
        notes
    });
}));

router.post('/', asyncWrapper(async (req, res) => {
    const { userId } = req.user;
    await addNotes(userId, req.body);
    res.json({message: 'Success'});
}));

router.get('/:id', asyncWrapper(async (req, res) => {
    const { userId } = req.user;
    const { id } = req.params;
    const note = await getNoteByIdForUser(id, userId);
    if(!note) {
        throw new InvalidRequestError('No notes found');
    }
    res.json({note});
}));

router.put('/:id', asyncWrapper(async (req, res) => {
    const { userId } = req.user;
    const { id } = req.params;
    const { text } = req.body;

    await updateNoteByIdForUser(id, userId, text);
    res.json({message: 'Success'});
}));

router.patch('/:id', asyncWrapper(async (req, res) => {
    const { userId } = req.user;
    const { id } = req.params;
    
    await toggleCompletedForUserNoteById(id, userId);
    res.json({message: 'Success'});
}));

router.delete('/:id', asyncWrapper(async (req, res) => {
    const { userId } = req.user;
    const { id } = req.params;
    await deleteNoteByIdForUser(id, userId);
    res.json({message: 'Success'});
}));

module.exports = {
    notesRouter: router
}
