const mongoose = require('mongoose');

const Note = mongoose.model('Note', {
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    },
    text: {
        type: String,
        required: true
    },
    completed: {
        type: Boolean,
        required: true,
        enum: [true, false],
        default: false
    },

    createdDate: {
        type: Date,
        default: Date.now()
    }
});

module.exports = {
    Note
};